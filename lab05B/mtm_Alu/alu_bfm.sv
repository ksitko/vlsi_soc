interface alu_bfm;
	import alu_pkg::*;

	bit    clk;
	bit    rst_n;
	bit    sin;
	bit		sout;

	bit 		sample_flag;	
	packet_t 		rx_B3, rx_B2, rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD;
//------------------------------------------------------------------------------
// Clock generator
//------------------------------------------------------------------------------
	initial begin : clk_gen
		clk = 0;
		forever begin : clk_frv
			#10;
			clk = ~clk;
		end
	end
//------------------------------------------------------------------------------
// Tasks & functions
//------------------------------------------------------------------------------
/////////// From tester: start ///////////
	task reset_alu();
		rst_n   = 1'b0;
		sin     = 1'b1;
		@(negedge clk);
		@(negedge clk);
		rst_n   = 1'b1;
		@(negedge clk);
		@(negedge clk);
	endtask
////////////////////////////////
	function bit [3:0] calc_sin_crc(
			input  bit   [31:0]  B,
			input  bit   [31:0]  A,
			input  bit   [2:0]   op
		);
		// polynomial: x^4 + x^1 + 1
		// data width: 68
		// convention: the first serial bit is D[67]

		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;

		begin
			d            = {B, A, 1'b1, op};
			c            = 4'b0000;             //crc init to 0

			newcrc[0]    = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
			newcrc[1]    = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
			newcrc[2]    = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
			newcrc[3]    = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];

			return  newcrc;
		end

	endfunction : calc_sin_crc	
////////////////////////////////
	task send_packet(input packet_t packet);
		for (integer i = 0; i<11; i++) begin
			@(posedge clk);
			sin <= packet[10-i];
		end
	endtask
////////////////////////////////
	task DATA(input payload_u payload);
		begin
			@(posedge clk);
			sin <= START_BIT;

			@(posedge clk);
			sin <= T_DATA;

			for (shortint i = 7; i>=0; i--) begin
				@(posedge clk);
				sin <= payload.bits[i];
			end

			@(posedge clk);
			sin <= STOP_BIT;
		end
	endtask
////////////////////////////////
	task CMD(input bit [2:0] op, input bit [3:0] crc);
		begin
			@(posedge clk);
			sin <= START_BIT;

			@(posedge clk);
			sin <= T_CMD;

			@(posedge clk);
			sin <= 1'b0;

			for (shortint i = 2; i>=0; i--) begin
				@(posedge clk);
				sin <= op[i];
			end

			for (shortint i = 3; i>=0; i--) begin
				@(posedge clk);
				sin <= crc[i];
			end

			@(posedge clk);
			sin <= STOP_BIT;
		end
	endtask
////////////////////////////////
	task process_two_32bit_numbers (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [3:0] crc);
		begin
//		start_data_tx = 1'b1;
			DATA(B[31:24]);
			DATA(B[23:16]);
			DATA(B[15:8] );
			DATA(B[7:0]  );
			DATA(A[31:24]);
			DATA(A[23:16]);
			DATA(A[15:8] );
			DATA(A[7:0]  );
			CMD(op, crc);
//			start_data_tx = 1'b0;
		end
	endtask
/////////// From tester: end ///////////

//////// From scoreboard: start ////////
	task get_packet(output packet_t out);   //function must be envoked with always @(negedge sin) to properly find start_bit. Reads one packet from sin
		begin
			//start bit
			if (sin != 1'b0) begin
				@(negedge sin); //always in predict process is already sensitive on negedge sin so that's why this if is here - to not double negedge sin
			end
			out.start_bit = START_BIT; //no error can be found because sin always will be 0 (=start bit) after @(negedge sin)
			//t bit
			@(posedge clk);
			@(posedge clk); //posedge clk x2 because negedge sin was used above
			out.t_bit     = t_bit_t'(sin);
			//payload
			for (shortint i=7; i>=0; i--) begin
				@(posedge clk);
				out.payload.bits[i] = sin;
			end
			//stop bit
			@(posedge clk);
			if(sin==STOP_BIT) begin
				out.stop_bit = STOP_BIT;
			end
			else begin
				$error("ERROR: Stop bit was expected while reading packet from sin input. Time = %t", $time);
			end
		end
	endtask
////////////////////////////////
	task get_packets_for_32bit(output packet_t packet4, output packet_t packet3, output packet_t packet2, output packet_t packet1);
		begin
			get_packet(packet4); //from MSB
			get_packet(packet3);
			get_packet(packet2);
			get_packet(packet1); //to LSB
		end
	endtask
////////////////////////////////
	task get_CMD(output packet_t out);
		begin
			get_packet(out);
		end
	endtask
////////////////////////////////
	task get_B_and_A_and_CMD (  output packet_t rx_B3, output packet_t rx_B2, output packet_t rx_B1, output packet_t rx_B0,
			output packet_t rx_A3, output packet_t rx_A2, output packet_t rx_A1, output packet_t rx_A0,
			output packet_t rx_CMD);
		begin
			get_packets_for_32bit(rx_B3, rx_B2, rx_B1, rx_B0);
			get_packets_for_32bit(rx_A3, rx_A2, rx_A1, rx_A0);
			get_CMD(rx_CMD);
		end
	endtask
////////// From scoreboard: end //////////

////////// Command monitor: start //////////
command_monitor		command_monitor_h;
	always @(negedge sin) begin : command_monitor_thread

		get_B_and_A_and_CMD(rx_B3, rx_B2, rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD);
		sample_flag = ~sample_flag;

		command_monitor_h.write_to_monitor({rx_A3.payload, rx_A2.payload, rx_A1.payload, rx_A0.payload},
																				{rx_B3.payload, rx_B2.payload, rx_B1.payload, rx_B0.payload},
																				 operations_t'(rx_CMD.payload.CMD_bits.op),
																				 rx_CMD.payload.CMD_bits.crc);
	end : command_monitor_thread
////////// Command monitor: end ////////////

////////// Result monitor: start ////////////////
result_monitor 				result_monitor_h;
	initial begin : result_monitor_thread
		bit [54:0] result_tmp;
		predicted_queue_element_t result;
		forever begin
			@(negedge sout);
			for (int i = 54; i>=0; i--) begin
				@(posedge clk);	
				result_tmp [i] = sout;
			end		
			
			if (result_tmp[53] == 1'b1) begin
				result.err_flag = 1'b1;
				result.sout_predicted.C3 			= 11'b000_0000_0000;	//fill with zeros on purpose
				result.sout_predicted.C2 			= 11'b000_0000_0000;	//fill with zeros on purpose
				result.sout_predicted.C1 			= 11'b000_0000_0000;	//fill with zeros on purpose
				result.sout_predicted.C0 			= 11'b000_0000_0000;	//fill with zeros on purpose
				result.sout_predicted.C_CMD	= result_tmp[54:44];		//only CMD is on sout so it's assigned to be properly read in scoreboard write()
			end
			else begin
				result.err_flag = 1'b0;
				result.sout_predicted.C3 			= result_tmp[54:44];
				result.sout_predicted.C2 			= result_tmp[43:33];
				result.sout_predicted.C1			= result_tmp[32:22];
				result.sout_predicted.C0			= result_tmp[21:11];
				result.sout_predicted.C_CMD	= result_tmp[10:0];
			end
			
			result_monitor_h.write_to_monitor(result);
		end
	end : result_monitor_thread
////////// Result monitor: end /////////////////
endinterface : alu_bfm