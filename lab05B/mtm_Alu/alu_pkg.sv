package alu_pkg;
	import uvm_pkg::*;
	`include "uvm_macros.svh"
	
	parameter START_BIT = 1'b0;
	parameter STOP_BIT  = 1'b1;

	parameter T_DATA    = 1'b0;
	parameter T_CMD     = 1'b1;
    parameter AND       = 3'b000;
    parameter OR        = 3'b001;
    parameter WRONG_1   = 3'b010;
    parameter WRONG_2   = 3'b011;
    parameter ADD       = 3'b100;
    parameter SUB       = 3'b101;
    parameter WRONG_3   = 3'b110;
    parameter WRONG_4   = 3'b111;

	typedef enum bit {
		data    = T_DATA,
		cmd     = T_CMD
	} t_bit_t;

	typedef struct packed {
		bit carry;
		bit overflow;
		bit zero;
		bit negative;
	} flags_t;

	typedef struct packed {
		bit err_data;
		bit err_crc;
		bit err_op;
	} err_flags_t;

	typedef struct packed {
		bit b7;
		bit b6;
		bit b5;
		bit b4;
		bit b3;
		bit b2;
		bit b1;
		bit b0;
	} byte_t;

	typedef struct packed {
		bit zero;
		bit [2:0] op;
		bit [3:0] crc;
	} byte_CMD_t;

	typedef struct packed {
		bit zero;
		flags_t flags;
		bit [2:0] crc;
	} byte_C_CMD_t;

	typedef struct packed {
		bit one;
		err_flags_t err_flags;
		err_flags_t err_flags_duplicated;
		bit parity;
	} byte_error_C_CMD_t;

	typedef union packed {
		byte_t              bits;
		byte_CMD_t          CMD_bits;
		bit [7:0]           whole_byte;
		byte_C_CMD_t        C_CMD_bits;
		byte_error_C_CMD_t  error_C_CMD_bits;
	} payload_u;

	typedef struct packed {
		bit start_bit;      // '0'
		t_bit_t t_bit;      // data='0', cmd='1'
		payload_u payload;  // data
		bit stop_bit;       // '1'
	} packet_t;

	typedef enum bit [2:0] {
		and_op      		= 3'b000,
		or_op       		= 3'b001,
		wrong_op_1  	= 3'b010,
		wrong_op_2  	= 3'b011,
		add_op      		= 3'b100,
		sub_op     		= 3'b101,
		wrong_op_3  = 3'b110,
		wrong_op_4  = 3'b111
	} operations_t;

	typedef enum bit [2:0] {
		error_data_lenght   = 3'b000,
		error_cmd_zero_bit  = 3'b001,
		error_sin_crc       = 3'b010,
		error_wrong_op      = 3'b011,
		error_no_error      = 3'b111
	} error_t;

	typedef struct packed {
		packet_t    C3;
		packet_t    C2;
		packet_t    C1;
		packet_t    C0;
		packet_t    C_CMD;
	} sout_predicted_t;

	typedef struct {
		bit                 err_flag;       //0=no error is predicted, 1=error is predicted
		sout_predicted_t    sout_predicted; //only CMD if error predicted or C + CMD if no error
	} predicted_queue_element_t;


//includes must be at the end because they simply paste files
//so if they were at the beginning all of the declarations
//would not be visible and xrun could not compile

//config
`include "env_config.svh"
`include "alu_agent_config.svh"

//transactions 
`include "rnd_command.svh"
`include "min_max_command.svh"
`include "result_transaction.svh"

//tb components
`include "coverage.svh"
`include "tester.svh"
`include "scoreboard.svh"
`include "driver.svh"
`include "command_monitor.svh"
`include "result_monitor.svh"
`include "alu_agent.svh"
`include "env.svh"

//tests
`include "dual_test.svh"

endpackage