class min_max_command extends rnd_command;
	
	`uvm_object_utils(min_max_command)

	constraint data {
		A dist {32'h0000_0000:=1, 32'hFFFF_FFFF:=1};
 		B dist {32'h0000_0000:=1, 32'hFFFF_FFFF:=1};
	} 
	
	function new(string name="");super.new(name);endfunction

endclass : min_max_command