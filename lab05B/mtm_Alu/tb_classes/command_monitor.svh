class command_monitor extends uvm_component;
	
    `uvm_component_utils(command_monitor)

    virtual alu_bfm bfm;

    uvm_analysis_port #(rnd_command) ap;
//////////////////////////////////////////////
    function new (string name, uvm_component parent);
        super.new(name,parent);
    endfunction
//////////////////////////////////////////////
    function void build_phase(uvm_phase phase);
	    
	    alu_agent_config	alu_agent_config_h;
	    
        if(!uvm_config_db #(alu_agent_config)::get(this, "","config", alu_agent_config_h))
            `uvm_fatal("COMMAND MONITOR", "Failed to get CONFIG")
        
        alu_agent_config_h.bfm.command_monitor_h = this;
        
        ap	= new("ap",this);
    endfunction : build_phase
//////////////////////////////////////////////
    function void write_to_monitor( bit [31:0] A, bit [31:0] B, operations_t op, bit [3:0] crc);
	    rnd_command cmd;
 		`uvm_info("COMMAND MONITOR",$sformatf("MONITOR: A: %2h  B: %2h  op: %s  crc: %2h",
                A, B, op.name(), crc), UVM_HIGH);
	    cmd		= new("cmd");
	    cmd.A		= A;
	    cmd.B		= B;
	    cmd.op	= op;
	    cmd.crc	= crc;
        ap.write(cmd);
    endfunction : write_to_monitor
//////////////////////////////////////////////
endclass : command_monitor