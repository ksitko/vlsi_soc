class driver extends uvm_driver #(sequence_item);
	
    `uvm_component_utils(driver)

    virtual alu_bfm bfm;
////////////////////////////////	
	function bit [3:0] calc_sin_crc(
			input  bit   [31:0]  	B,
			input  bit   [31:0] 	A,
			input  bit   [2:0]   		op
		);
		// polynomial: x^4 + x^1 + 1
		// data width: 68
		// convention: the first serial bit is D[67]

		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;

		begin
			d            = {B, A, 1'b1, op};
			c            = 4'b0000;             //crc init to 0

			newcrc[0]    = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
			newcrc[1]    = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
			newcrc[2]    = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
			newcrc[3]    = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];

			return  newcrc;
		end

	endfunction : calc_sin_crc
////////////////////////////////
    function void build_phase(uvm_phase phase);
        if(!uvm_config_db #(virtual alu_bfm)::get(null, "*","bfm", bfm))
			`uvm_fatal("DRIVER", "Failed to get BFM")
    endfunction : build_phase
////////////////////////////////
    task run_phase(uvm_phase phase);
	    sequence_item cmd;
//	            void'(begin_tr(cmd));
	    bfm.reset_alu();
        forever begin : cmd_loop
	        seq_item_port.get_next_item(cmd);
	        cmd.crc = calc_sin_crc(cmd.B, cmd.A, cmd.op);
          	bfm.process_two_32bit_numbers(cmd.B, cmd.A, cmd.op, cmd.crc);
	        seq_item_port.item_done();
        end : cmd_loop
//               end_tr(cmd);
    endtask : run_phase
////////////////////////////////
    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new
////////////////////////////////
endclass : driver
