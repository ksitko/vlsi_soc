class sequence_item extends uvm_sequence_item;
	
	rand bit     [31:0]  	A;
	rand bit     [31:0]  	B;
	rand operations_t		op;
	bit     			[3:0]   		crc;

////////////////////////////////////////
   function new (string name = "sequence_item");
      super.new(name);
   endfunction : new
////////////////////////////////////////
   `uvm_object_utils_begin(sequence_item)
	   `uvm_field_int(A, UVM_ALL_ON)
	   `uvm_field_int(B, UVM_ALL_ON)
	   `uvm_field_enum(operations_t, op, UVM_ALL_ON)
	   `uvm_field_int(crc, UVM_ALL_ON)
   `uvm_object_utils_end
////////////////////////////////////////
   function string convert2string();
      string s;
      s = $sformatf("A: %h  B: %h op: %s crc: %h",
                        A, B, op.name(), crc);
      return s;
   endfunction : convert2string
////////////////////////////////////////
endclass