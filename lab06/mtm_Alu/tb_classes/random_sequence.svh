class random_sequence extends uvm_sequence #(sequence_item);
	
   `uvm_object_utils (random_sequence)

   sequence_item		command;
////////////////////////////////
    function new(string name = "random_sequence");
        super.new(name);
    endfunction : new
////////////////////////////////
	task body();
		`uvm_info("SEQ_RANDOM","",UVM_MEDIUM)
		
		repeat(1000) begin
			`uvm_do(command)
			
			#100;
		end
	endtask
////////////////////////////////	
endclass 