class minmax_sequence extends uvm_sequence #(sequence_item);
    `uvm_object_utils(minmax_sequence)

   sequence_item		command;
////////////////////////////////
    function new(string name = "random_sequence");
        super.new(name);
    endfunction : new
////////////////////////////////
	task body();
	    `uvm_info("SEQ_MINMAX","",UVM_MEDIUM)
	    
	    repeat(50) begin
		`uvm_do_with(command, {	A dist {32'h0000_0000:=1, 32'hFFFF_FFFF:=1};
 																B dist {32'h0000_0000:=1, 32'hFFFF_FFFF:=1};
															})
			#100;
	    	end
    endtask
////////////////////////////////
endclass