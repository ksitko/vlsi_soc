class scoreboard extends uvm_subscriber #(result_transaction);
	
	`uvm_component_utils(scoreboard)
	
	uvm_tlm_analysis_fifo #(sequence_item) cmd_f;
////////////////////////////////
	function new (string name, uvm_component parent);
        super.new(name, parent);
	endfunction : new
////////////////////////////////	
	function void build_phase(uvm_phase phase);
        cmd_f = new ("cmd_f", this);
	endfunction : build_phase
////////////////////////////////
	function bit [3:0] calc_sin_crc(
			input  bit   [31:0]  B,
			input  bit   [31:0]  A,
			input  bit   [2:0]   op
		);
		// polynomial: x^4 + x^1 + 1
		// data width: 68
		// convention: the first serial bit is D[67]

		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;

		begin
			d            = {B, A, 1'b1, op};
			c            = 4'b0000;             //crc init to 0

			newcrc[0]    = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
			newcrc[1]    = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
			newcrc[2]    = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
			newcrc[3]    = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];

			return  newcrc;
		end

	endfunction : calc_sin_crc	
////////////////////////////////
	function flags_t calc_flags (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [31:0] C);
		flags_t flags;
		flags.carry     = 1'b0;
		flags.overflow  = 1'b0;
		flags.zero      = 1'b0;
		flags.negative  = 1'b0;
		begin
			if (op == add_op) begin
				if ((A[31] & B[31]) | (A[31] & ~C[31]) | (B[31] & ~C[31]))  flags.carry    = 1'b1;
				if ((A[31] & B[31] & ~C[31]) | (~A[31] & ~B[31] & C[31]))   flags.overflow = 1'b1;
			end
			if (op == sub_op) begin
				if ((A[31] & ~B[31]) | (A[31] & C[31]) | (~B[31] & C[31]))  flags.carry    = 1'b1;
				if ((~A[31] & B[31] & ~C[31]) | (A[31] & ~B[31] & C[31]))   flags.overflow = 1'b1;
			end
			if (C == 32'h0000_0000)                                         flags.zero     = 1'b1;
			if (C[31] == 1'b1)                                              flags.negative = 1'b1;

			return flags;
		end
	endfunction
////////////////////////////////
	function calc_parity(input packet_t C_CMD);
		return ^C_CMD.payload[7:1];     //reduction XOR
	endfunction
////////////////////////////////
	function [2:0] calc_sout_crc(input [31:0] C, input flags_t flags);
		// polynomial: x^3 + x^1 + 1
		// data width: 37
		// convention: the first serial bit is D[36]
		reg [36:0]  d;
		reg [2:0]   c;
		reg [2:0]   newcrc;

		begin
			d         = {C, 1'b0, flags};
			c         = 3'b000;

			newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
			newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
			newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
			return newcrc;
		end
	endfunction
////////////////////////////////
	function result_transaction predict_result (sequence_item cmd);
		
		flags_t            	flags;
		bit [2:0]        	sout_crc;
		err_flags_t     	err_flags;
		bit             	parity;
		bit [3:0]        crc_calculated_from_sin;
		bit [31:0] 		C;
		packet_t 		C_CMD;
		
		result_transaction	predicted;
		predicted = new("predicted");
		
		begin
			//clear
			flags.carry             		= 1'b0;
			flags.overflow          	= 1'b0;
			flags.zero              		= 1'b0;
			flags.negative          	= 1'b0;
			err_flags.err_crc       	= 1'b0;
			err_flags.err_op        	= 1'b0;
			err_flags.err_data		= 1'b0;
			sout_crc                		= 3'b000;
			crc_calculated_from_sin = 4'b000;
			/***************************************/
			/******** Calculating error flags *******/
			/***************************************/
			/* --- /ERR_CRC ---*/
			crc_calculated_from_sin = calc_sin_crc(cmd.B, cmd.A, cmd.op);

			if(crc_calculated_from_sin != cmd.crc) begin
				err_flags.err_crc                                   = 1'b1;
			end
			/* --- ERR_OP --- */
			if(cmd.op[1] == 1'b1) begin
				err_flags.err_op                                    = 1'b1;
			end
			/***************************************/
			/*********** Predicting result **********/
			/***************************************/
			C_CMD.start_bit         = START_BIT;
			C_CMD.t_bit             = t_bit_t'(T_CMD);
			C_CMD.stop_bit          = STOP_BIT;
			if (err_flags != 3'b000) begin
				C_CMD.payload.error_C_CMD_bits.one                  				= 1'b1;
				C_CMD.payload.error_C_CMD_bits.err_flags            			= err_flags;
				C_CMD.payload.error_C_CMD_bits.err_flags_duplicated 	= err_flags;
				C_CMD.payload.error_C_CMD_bits.parity               				= calc_parity(C_CMD);
			end
			else begin
				/* --- Calculate C --- */
				case(cmd.op)
					AND:   	C = cmd.B & cmd.A;
					OR:    	C = cmd.B | cmd.A;
					ADD:	C = cmd.B + cmd.A;
					SUB:   	C = cmd.B - cmd.A;
				endcase
				/* --- Set CMD MSB to zero --- */
				C_CMD.payload.C_CMD_bits.zero		= 1'b0;
				/* --- Calculate flags --- */
				flags                                               			= calc_flags(cmd.B, cmd.A, cmd.op, C);
				C_CMD.payload.C_CMD_bits.flags    = flags;
				/* --- Calculate sout crc --- */
				C_CMD.payload.C_CMD_bits.crc       = calc_sout_crc(C, flags);
			end
			/***************************************/
			/********** Predicted result  ***********/
			/***************************************/			
			if(C_CMD.payload[7] == 1'b1) begin  //by MSB it can be distinguished if error occured or not
				predicted.result.err_flag              = 1'b1;
			end
			else begin
				predicted.result.err_flag              			= 1'b0;
				predicted.result.sout_predicted.C3     	= {START_BIT, T_DATA, C[31:24], STOP_BIT};
				predicted.result.sout_predicted.C2     	= {START_BIT, T_DATA, C[23:16], STOP_BIT};
				predicted.result.sout_predicted.C1     	= {START_BIT, T_DATA, C[15:8 ], STOP_BIT};
				predicted.result.sout_predicted.C0     	= {START_BIT, T_DATA, C[7:0  ], STOP_BIT};
			end
			predicted.result.sout_predicted.C_CMD 	= C_CMD;
			
			return predicted;
			
		end
	endfunction 
////////////////////////////////
	function void write (result_transaction t);
		string data_str;
		sequence_item cmd;
		result_transaction predicted;
		
		if(!cmd_f.try_get(cmd))		$fatal(1, "Missing command in self checker");

		predicted = predict_result(cmd);

        data_str  = {cmd.convert2string(), " ==>  Actual ", t.convert2string(), "/Predicted ", predicted.convert2string()};

        if (!predicted.compare(t))
            `uvm_error("SELF CHECKER", {"FAIL: ",data_str})
        else
            `uvm_info ("SELF CHECKER", {"PASS: ", data_str}, UVM_HIGH)
            
	endfunction 
////////////////////////////////
endclass : scoreboard