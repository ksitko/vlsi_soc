class rnd_command extends uvm_transaction;
	
	`uvm_object_utils(rnd_command)

	rand bit     [31:0]  	A;
	rand bit     [31:0]  	B;
	rand operations_t		op;
	bit     			[3:0]   		crc;

////////////////////////////////////////
   function void do_copy(uvm_object rhs);
      rnd_command copied_transaction_h;

      if(rhs == null) 
        `uvm_fatal("COMMAND TRANSACTION", "Tried to copy from a null pointer")
      
      super.do_copy(rhs); // copy all parent class data

      if(!$cast(copied_transaction_h,rhs))
        `uvm_fatal("COMMAND TRANSACTION", "Tried to copy wrong type.")

      A	= copied_transaction_h.A;
      B	= copied_transaction_h.B;
      op	= copied_transaction_h.op;
      crc	= copied_transaction_h.crc;
      
   endfunction : do_copy		
////////////////////////////////////////
   function rnd_command clone_me();
      rnd_command clone;
      uvm_object tmp;

      tmp = this.clone();
      $cast(clone, tmp);
      return clone;
   endfunction : clone_me
////////////////////////////////////////
   function bit do_compare(uvm_object rhs, uvm_comparer comparer);
      rnd_command compared_transaction_h;
      bit   same;
      
      if (rhs==null) `uvm_fatal("RANDOM TRANSACTION", 
                                "Tried to do comparison to a null pointer");
      
      if (!$cast(compared_transaction_h,rhs))
        same = 0;
      else
        same = super.do_compare(rhs, comparer) && 
               (compared_transaction_h.A == A) &&
               (compared_transaction_h.B == B) &&
               (compared_transaction_h.op == op) &&
               (compared_transaction_h.crc == crc);
               
      return same;
   endfunction : do_compare		
////////////////////////////////////////
   function string convert2string();
      string s;
      s = $sformatf("A: %h  B: %h op: %s crc: %h",
                        A, B, op.name(), crc);
      return s;
   endfunction : convert2string
////////////////////////////////////////
   function new (string name = "");
      super.new(name);
   endfunction : new
////////////////////////////////////////
endclass : rnd_command