class driver extends uvm_component;
	
    `uvm_component_utils(driver)

    virtual alu_bfm bfm;
	
    uvm_get_port #(rnd_command) command_port;
////////////////////////////////
    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new
////////////////////////////////
    function void build_phase(uvm_phase phase);
        if(!uvm_config_db #(virtual alu_bfm)::get(null, "*","bfm", bfm))
			`uvm_fatal("DRIVER", "Failed to get BFM")
        command_port = new("command_port",this);
    endfunction : build_phase
////////////////////////////////
    task run_phase(uvm_phase phase);
	    bit     [31:0]  	A;
		bit     [31:0]  	B;
		operations_t		op;
	    
        rnd_command command;
	    
	    bfm.reset_alu();
        forever begin : command_loop
            command_port.get(command);
          	bfm.process_two_32bit_numbers(command.B, command.A, command.op, command.crc);
        end : command_loop
    endtask : run_phase
////////////////////////////////
endclass : driver
