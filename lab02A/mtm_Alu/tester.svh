module tester(alu_bfm bfm);
	import alu_pkg::*;
////////////////////////////////
	function bit [31:0] get_data();
		bit [2:0] zero_ones;
		zero_ones = $random;
		if (zero_ones == 3'b000)
			return 32'h0000_0000;
		else if (zero_ones == 3'b111)
			return 32'hFFFF_FFFF;
		else
			return $random;
	endfunction : get_data
////////////////////////////////
	function operations_t get_op();
		bit [3:0] op_choice;
		op_choice = $random;
		case(op_choice)
			1, 6, 11:   return and_op;
			2, 7, 12:   return or_op;
			3, 8, 13:   return add_op;
			4, 9, 14:   return sub_op;
			0:          return wrong_op_1;
			5:          return wrong_op_2;
			10:         return wrong_op_3;
			15:         return wrong_op_4;
		endcase
	endfunction :get_op
////////////////////////////////
//------------------------
// Tester main
	initial begin : tester
		bit     [31:0]  A;
		bit     [31:0]  B;
		operations_t    op;
		bit     [3:0]   crc;


		bfm.reset_alu();
		repeat (1000) begin : tester_main
			B   = get_data();
			A   = get_data();
			op  = get_op();
			crc	= bfm.calc_sin_crc(B, A , op);

			bfm.process_two_32bit_numbers(B, A, op, crc);

			#100;
		end : tester_main

		#1000;
		$finish;
	end : tester
//------------------------
endmodule :tester