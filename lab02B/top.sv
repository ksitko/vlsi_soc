virtual class shape;
	real width 	= -1;
	real height = -1;
	
	function new(real w, real h);
		width 	= w;
		height 	= h;
	endfunction 
	
	pure virtual function real get_area();
	pure virtual function void print();
	
endclass  : shape
/////////////////////////////////////////////////////////////////////////////////////////
class rectangle extends shape;
	
	function new(real width, real height);
		super.new(.w(width), .h(height));
	endfunction 
	
	function real get_area();
		return width*height;
	endfunction : get_area
	
	function void print();
		$display("Rectangle w=%g h=%g area=%g", width, height, get_area());
	endfunction : print
	
endclass : rectangle
/////////////////////////////////////////////////////////////////////////////////////////
class square extends rectangle;
	
	function new(real side);
		super.new(.width(side), .height(side));
	endfunction
	
	function void print();
		$display("Square w=%g area=%g", width, get_area());
	endfunction : print
	
endclass : square
/////////////////////////////////////////////////////////////////////////////////////////
class triangle extends shape;
	
	function new(real width, real height);
		super.new(.w(width), .h(height));
	endfunction 
	
	function real get_area();
		return 0.5*width*height;
	endfunction : get_area
	
	function void print();
		$display("Triangle w=%g h=%g area=%g", width, height, get_area());
	endfunction : print
	
endclass : triangle
/////////////////////////////////////////////////////////////////////////////////////////
class shape_factory;
	
	static function shape make_shape(string shape_type, real w, real h);
	
		rectangle 	rectangle_h;
		square		square_h;
		triangle	triangle_h;
	
		case(shape_type)
			"rectangle" : begin
				rectangle_h	= new(w, h);
				return rectangle_h;
			end
			
			"square" : begin
				square_h	= new(w);
				return square_h;
			end
			
			"triangle" : begin
				triangle_h	= new(w, h);
				return triangle_h;
			end
		
			default : 
				$fatal(1, {"No such shape: ", shape_type});
		endcase
		
	endfunction : make_shape
	
endclass : shape_factory
/////////////////////////////////////////////////////////////////////////////////////////
class shape_reporter #(type T=shape);
	
	protected static T shape_storage [$];
	
	static function void store_shape(T l);
		shape_storage.push_back(l);
	endfunction : store_shape 
	
	static function void report_shapes();
		real total_area = 0.0;
		
		foreach (shape_storage[i]) begin
			shape_storage[i].print();
			total_area = total_area + shape_storage[i].get_area();
		end
		$display("Total area: %g", total_area);
	endfunction : report_shapes
	
endclass : shape_reporter
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
module top;
	initial begin
		shape		shape_h;
		rectangle	rectangle_h;
		square		square_h;
		triangle	triangle_h;
		
		int fd;			//file descriptor (handler to file)
		string line;	
		
		string shape;	//shape will be read from file to this variable
		real w, h;		//width and height will be read from file to this  variables
				
		$display; $display; $display;
		$display("#########################################");
		$display("#####      Starting simulation      #####");
		$display("#########################################");
		
		//open file
		fd = $fopen("./lab02B_shapes.txt", "r");	
		if(!fd) $fatal(1, "File was NOT opened successfully: %d", fd);
		
		//read from file and store shapes 
		while($fscanf(fd, "%s%f%f", shape, w, h)==3) begin //==3 because it scans 3 values from each line from file
						
			shape_h = shape_factory::make_shape(shape, w, h);	
			
			case(shape)
				"rectangle" : begin
					$cast(rectangle_h, shape_h);
					shape_reporter#(rectangle)::store_shape(rectangle_h);
				end
				"square" 	: begin
					$cast(square_h, shape_h);
					shape_reporter#(square)::store_shape(square_h);
				end
				"triangle" 	: begin
					$cast(triangle_h, shape_h);
					shape_reporter#(triangle)::store_shape(triangle_h);
				end
				default		: break;
			endcase

		end
		
		//report shapes
		shape_reporter#(rectangle)::report_shapes();
		$display;
		shape_reporter#(square)::report_shapes();
		$display;
		shape_reporter#(triangle)::report_shapes();
		
		$display("#########################################");
		$display("#####     Finishing simulation      #####");
		$display("#########################################");
		$display;
		
		//close file
		$fclose(fd);	
		$finish;
	end
endmodule : top