# tested with XCELIUM 18.09

# Commmand arguments:
# -g option starts xrun simulation with gui, with separate database

# To set the paths for xrun execute the following command in the terminal:
# source /cad/env/cadence_path.XCELIUM1809

# Help library if available with command:
# cdnshelp &

#------------------------------------------------------------------------------
# local variables#<<<
INCA="INCA_libs"
GUI=""
#>>>
#------------------------------------------------------------------------------
# check input script arguments#<<<
while getopts gh option
  do case "${option}" in
    g) GUI="+access+r +gui"; INCA="${INCA}_gui";;
    *) echo "The only valid option is -g (for GUI)"; exit -1 ;;
  esac
done
#------------------------------------------------------------------------------
# init #<<<
rm -rf $INCA      # remove previous database

#------------------------------------------------------------------------------
# PROCEDURES
#------------------------------------------------------------------------------

# run the main
xrun -top top -f dut.f $GUI