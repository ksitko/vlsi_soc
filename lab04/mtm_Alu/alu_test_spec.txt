General test spocifcation:
- Completely test the ALU functionality,
- Execute all lines of the ALU code.
###########################################################
Detailed test specification:
A. Operations & sequences
	1. Test all operations
	2. First do numerical operation then logical
	3. Logical operation then numerical
	4. Wrong operation then correct operation 
	5. Execute 3 logical operations in row
	6. Execute 3 numerical operations in row
	7. Execute all operations after reset
B. Specific data corners
	1. Simulate all zero input for all the operations
	2. Simulate all one input for all the operations
