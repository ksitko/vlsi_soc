class coverage extends uvm_subscriber #(command_s);

	`uvm_component_utils(coverage)

	bit     [31:0]  	A;
	bit     [31:0]  	B;
	operations_t	op_set;
	
// ---------------------------- //
//	A - Operations & sequences  //
// ---------------------------- //
	covergroup op_cov;
		option.name = "op_cov";

		coverpoint op_set {			
			//A1 Test all operations
			bins A1_all[] = {[and_op : wrong_op_4]};
			
			//A2 Numerical operation then logical 
			bins A2_num_then_logic[] = (add_op, sub_op => and_op, or_op);
			
			//A3 Logical operation then numerical
			bins A3_logic_then_num[] = (and_op, or_op => add_op, sub_op);
			
			//A4 Wrong operation then correct operation
			bins A4_wrong_op_then_corr_op[]	= (wrong_op_1, wrong_op_2, wrong_op_3, wrong_op_4 => and_op, or_op, add_op, sub_op);
			
			//A5 Execute 3 logical operations in row
			bins A5_3_logical_in_row[] 	= ([and_op:or_op]  [* 2]);
			
			//A6 Execute 3 numerical operations in row
			bins A6_3_num_in_row[] 	= ([add_op:sub_op] [* 2]);
		}
	endgroup 
// ---------------------------- //
//	B - Specific data corners   //
// ---------------------------- //
	covergroup zeros_or_ones_on_ops;
		
		option.name = "zeros_or_ones_on_ops";
		
		all_ops: coverpoint op_set {}
		
		a_leg: coverpoint A {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}
		
		b_leg: coverpoint B {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}
		
		B_op_00_FF: cross a_leg, b_leg, all_ops {
			
			//B1 Simulate all zero input for all the operations
			bins B1_all_ops_00 = binsof (all_ops) && (binsof(a_leg.zeros) || binsof(b_leg.zeros));
			
			//B2 Simulate all one input for all the operations
			bins B1_all_ops_FF = binsof (all_ops) && (binsof(a_leg.ones) || binsof(b_leg.ones));
			
			ignore_bins others_only = binsof(a_leg.others) && binsof(b_leg.others);
		}
	endgroup 
////////////////////////////////
	function new (string name, uvm_component parent);
		super.new(name, parent);
		op_cov								= new();
		zeros_or_ones_on_ops	= new();
	endfunction : new	
////////////////////////////////
	function void write (command_s t);
		A				= t.A;
		B				= t.B;
		op_set 	= t.op;
		op_cov.sample();
		zeros_or_ones_on_ops.sample();
	endfunction : write
////////////////////////////////
endclass : coverage
