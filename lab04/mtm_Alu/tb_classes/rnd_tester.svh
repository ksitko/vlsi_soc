class rnd_tester extends base_tester;
	
	`uvm_component_utils (rnd_tester)
    
    virtual function operations_t get_op();
		bit [3:0] op_choice;
		op_choice = $random;
		case(op_choice)
			1, 6, 11:	return and_op;
			2, 7, 12:	return or_op;
			3, 8, 13:	return add_op;
			4, 9, 14:	return sub_op;
			0:          	return wrong_op_1;
			5:          	return wrong_op_2;
			10:         	return wrong_op_3;
			15:         	return wrong_op_4;
		endcase
	endfunction : get_op

	virtual function bit [31:0] get_data();
		return $random;
	endfunction : get_data
	
	function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new
	
endclass : rnd_tester