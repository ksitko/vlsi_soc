class scoreboard extends uvm_subscriber #(bit [54:0]);
	
	`uvm_component_utils(scoreboard)
	
	virtual alu_bfm bfm;
	uvm_tlm_analysis_fifo #(command_s) cmd_f;
	
	function new (string name, uvm_component parent);
        super.new(name, parent);
	endfunction : new
	
	function void build_phase(uvm_phase phase);
        cmd_f = new ("cmd_f", this);
	endfunction : build_phase

////////////////////////////////
	function bit [3:0] calc_sin_crc(
			input  bit   [31:0]  B,
			input  bit   [31:0]  A,
			input  bit   [2:0]   op
		);
		// polynomial: x^4 + x^1 + 1
		// data width: 68
		// convention: the first serial bit is D[67]

		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;

		begin
			d            = {B, A, 1'b1, op};
			c            = 4'b0000;             //crc init to 0

			newcrc[0]    = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
			newcrc[1]    = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
			newcrc[2]    = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
			newcrc[3]    = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];

			return  newcrc;
		end

	endfunction : calc_sin_crc	
////////////////////////////////
	function flags_t calc_flags (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [31:0] C);
		flags_t flags;
		flags.carry     = 1'b0;
		flags.overflow  = 1'b0;
		flags.zero      = 1'b0;
		flags.negative  = 1'b0;
		begin
			if (op == add_op) begin
				if ((A[31] & B[31]) | (A[31] & ~C[31]) | (B[31] & ~C[31]))  flags.carry    = 1'b1;
				if ((A[31] & B[31] & ~C[31]) | (~A[31] & ~B[31] & C[31]))   flags.overflow = 1'b1;
			end
			if (op == sub_op) begin
				if ((A[31] & ~B[31]) | (A[31] & C[31]) | (~B[31] & C[31]))  flags.carry    = 1'b1;
				if ((~A[31] & B[31] & ~C[31]) | (A[31] & ~B[31] & C[31]))   flags.overflow = 1'b1;
			end
			if (C == 32'h0000_0000)                                         flags.zero     = 1'b1;
			if (C[31] == 1'b1)                                              flags.negative = 1'b1;

			return flags;
		end
	endfunction
////////////////////////////////
	function calc_parity(input packet_t C_CMD);
		return ^C_CMD.payload[7:1];     //reduction XOR
	endfunction
////////////////////////////////
	function [2:0] calc_sout_crc(input [31:0] C, input flags_t flags);
		// polynomial: x^3 + x^1 + 1
		// data width: 37
		// convention: the first serial bit is D[36]
		reg [36:0]  d;
		reg [2:0]   c;
		reg [2:0]   newcrc;

		begin
			d         = {C, 1'b0, flags};
			c         = 3'b000;

			newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
			newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
			newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
			return newcrc;
		end
	endfunction
////////////////////////////////
	function void predict_result ( 
			input bit [31:0] A,
			input bit [31:0] B,
			input bit [2:0] CMD_op,
			input bit [3:0] CMD_crc,
			output predicted_queue_element_t predicted_result);
		
		flags_t            	flags;
		bit [2:0]        	sout_crc;
		err_flags_t     	err_flags;
		bit             	parity;
		bit [3:0]        crc_calculated_from_sin;
		bit [31:0] 		C;
		packet_t 		C_CMD;
		
		begin
			//clear
			flags.carry             		= 1'b0;
			flags.overflow          	= 1'b0;
			flags.zero              		= 1'b0;
			flags.negative          	= 1'b0;
			err_flags.err_crc       	= 1'b0;
			err_flags.err_op        	= 1'b0;
			err_flags.err_data		= 1'b0;
			sout_crc                		= 3'b000;
			crc_calculated_from_sin = 4'b000;
			/***************************************/
			/******** Calculating error flags *******/
			/***************************************/
			/* --- /ERR_CRC ---*/
			crc_calculated_from_sin = calc_sin_crc(B, A, CMD_op);

			if(crc_calculated_from_sin != CMD_crc) begin
				err_flags.err_crc                                   = 1'b1;
			end
			/* --- ERR_OP --- */
			if(CMD_op[1] == 1'b1) begin
				err_flags.err_op                                    = 1'b1;
			end
			/***************************************/
			/*********** Predicting result **********/
			/***************************************/
			C_CMD.start_bit         = START_BIT;
			C_CMD.t_bit             = t_bit_t'(T_CMD);
			C_CMD.stop_bit          = STOP_BIT;
			if (err_flags != 3'b000) begin
				C_CMD.payload.error_C_CMD_bits.one                  = 1'b1;
				C_CMD.payload.error_C_CMD_bits.err_flags            = err_flags;
				C_CMD.payload.error_C_CMD_bits.err_flags_duplicated = err_flags;
				C_CMD.payload.error_C_CMD_bits.parity               = calc_parity(C_CMD);
			end
			else begin
				/* --- Calculate C --- */
				case(CMD_op)
					AND:   	C = B & A;
					OR:    	C = B | A;
					ADD:	C = B + A;
					SUB:   	C = B - A;
				endcase
				/* --- Set CMD MSB to zero --- */
				C_CMD.payload.C_CMD_bits.zero		= 1'b0;
				/* --- Calculate flags --- */
				flags                                               			= calc_flags(B, A, CMD_op, C);
				C_CMD.payload.C_CMD_bits.flags    = flags;
				/* --- Calculate sout crc --- */
				C_CMD.payload.C_CMD_bits.crc       = calc_sout_crc(C, flags);
			end
			/***************************************/
			/********** Predicted result  ***********/
			/***************************************/			
			if(C_CMD.payload[7] == 1'b1) begin  //by MSB it can be distinguished if error occured or not
				predicted_result.err_flag              = 1'b1;
			end
			else begin
				predicted_result.err_flag              = 1'b0;
				predicted_result.sout_predicted.C3     = {START_BIT, T_DATA, C[31:24], STOP_BIT};
				predicted_result.sout_predicted.C2     = {START_BIT, T_DATA, C[23:16], STOP_BIT};
				predicted_result.sout_predicted.C1     = {START_BIT, T_DATA, C[15:8 ], STOP_BIT};
				predicted_result.sout_predicted.C0     = {START_BIT, T_DATA, C[7:0  ], STOP_BIT};
			end
			predicted_result.sout_predicted.C_CMD  = C_CMD;
			
		end
	endfunction 
////////////////////////////////
	function void write (bit [54:0] t);
		bit error_indicator;
		
		predicted_queue_element_t predicted_result;
		
		command_s cmd;
		cmd.A		= 0;
		cmd.B 	= 0;
		cmd.op	= operations_t'(3'b000);
		cmd.crc	= 4'b0000;
		if(!cmd_f.try_get(cmd))
			  $fatal(1, "Missing command in self checker");
		
		predict_result(cmd.A, cmd.B, cmd.op, cmd.crc, predicted_result);
		
		if (predicted_result.err_flag) begin
			if (predicted_result.sout_predicted.C_CMD != t[54:44]) begin
				$error("FAILED. Time = %t", $time);
			end
			else begin
				//$display("PASSED!!!!!!!!!!!!!!!!!!!!!");
			end
		end
		else begin
			if (predicted_result.sout_predicted != t) begin
				$error("FAILED. Time = %t", $time);
			end
			else begin
				//$display("PASSED!!!!!!!!!!!!!!!!!!!!!");
			end
		end
	endfunction 
	
endclass : scoreboard