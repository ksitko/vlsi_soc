class coverage extends uvm_component;

	`uvm_component_utils(coverage)

	virtual alu_bfm bfm;
	
// ---------------------------- //
//	A - Operations & sequences  //
// ---------------------------- //
	covergroup op_cov;
		option.name = "op_cov";

		coverpoint bfm.rx_CMD.payload.CMD_bits.op {			
			//A1 Test all operations
			bins A1_all[] = {[and_op : wrong_op_4]};
			
			//A2 Numerical operation then logical 
			bins A2_num_then_logic[] = (add_op, sub_op => and_op, or_op);
			
			//A3 Logical operation then numerical
			bins A3_logic_then_num[] = (and_op, or_op => add_op, sub_op);
			
			//A4 Wrong operation then correct operation
			bins A4_wrong_op_then_corr_op[]	= (wrong_op_1, wrong_op_2, wrong_op_3, wrong_op_4 => and_op, or_op, add_op, sub_op);
			
			//A5 Execute 3 logical operations in row
			bins A5_3_logical_in_row[] 	= ([and_op:or_op]  [* 2]);
			
			//A6 Execute 3 numerical operations in row
			bins A6_3_num_in_row[] 	= ([add_op:sub_op] [* 2]);
			
			//A7 execute all operations after reset
			//bins A7_all_after_rst[] = (bfm.rst_n => and_op, or_op, add_op, sub_op);
		}
	endgroup 
// ---------------------------- //
//	B - Specific data corners   //
// ---------------------------- //
	covergroup zeros_or_ones_on_ops;
		
		option.name = "zeros_or_ones_on_ops";
 
		all_ops: coverpoint bfm.rx_CMD.payload.CMD_bits.op {}
		
		a_leg: coverpoint {bfm.rx_A3.payload, bfm.rx_A2.payload, bfm.rx_A1.payload, bfm.rx_A0.payload} {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}
		
		b_leg: coverpoint {bfm.rx_B3.payload, bfm.rx_B2.payload, bfm.rx_B1.payload, bfm.rx_B0.payload} {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}
		
		B_op_00_FF: cross a_leg, b_leg, all_ops {
			
			//B1 Simulate all zero input for all the operations
			bins B1_all_ops_00 = binsof (all_ops) && (binsof(a_leg.zeros) || binsof(b_leg.zeros));
			
			//B2 Simulate all one input for all the operations
			bins B1_all_ops_FF = binsof (all_ops) && (binsof(a_leg.ones) || binsof(b_leg.ones));
			
			ignore_bins others_only = binsof(a_leg.others) && binsof(b_leg.others);
		}
	endgroup
////////////////////////////////
	function void build_phase(uvm_phase phase);
        if(!uvm_config_db #(virtual alu_bfm)::get(null, "*","bfm", bfm))
            $fatal(1,"Failed to get BFM");
        //op_cov					= new();
		//zeros_or_ones_on_ops	= new();
	endfunction : build_phase
	
	function new (string name, uvm_component parent);
		super.new(name, parent);
		op_cov					= new();
		zeros_or_ones_on_ops	= new();
	endfunction : new	
////////////////////////////////
////////////////////////////////
////////////////////////////////
	task run_phase(uvm_phase phase);
		forever begin : sample_cov
			@(posedge bfm.sample_flag or negedge bfm.sample_flag);
			op_cov.sample();
			zeros_or_ones_on_ops.sample();
		end :sample_cov
	endtask : run_phase
////////////////////////////////
endclass : coverage
