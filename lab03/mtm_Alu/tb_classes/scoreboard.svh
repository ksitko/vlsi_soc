class scoreboard extends uvm_component;
	
	`uvm_component_utils(scoreboard)
	
	virtual alu_bfm bfm;
	
	function new (string name, uvm_component parent);
        super.new(name, parent);
	endfunction : new
	
	function void build_phase(uvm_phase phase);
        if(!uvm_config_db #(virtual alu_bfm)::get(null, "*","bfm", bfm))
            $fatal(1,"Failed to get BFM");
    endfunction : build_phase
	
	
	task run_phase(uvm_phase phase);
		predicted_queue_element_t   predicted_sout; //pop_front from queue to this variable
		bit error_indicator;
		
		forever begin : self_checker
			@(negedge bfm.sout)
			error_indicator = 1'b0;
			predicted_sout  = bfm.predicted_values_queue.pop_front();    //Dequeue predicted value
			
			@(posedge bfm.clk);     //wait to next (posedge clk) because (negedge sout) was used above and this posedge refers to the same time moment
	
			if (predicted_sout.err_flag == 1'b1) begin : predicted_sout_with_err
				for (shortint i = 9; i>=0; i--) begin       //10 iterations because first start_bit is handled by always (@negedge sout) triggering and CMD is an 11-bit packet
					//also it's important to iterate on CMD not C3..C0 packets which are all in one structure. CMD is last so it's on LSB bits
					@(posedge bfm.clk);
					if(bfm.sout != predicted_sout.sout_predicted[i]) begin
						//$display("FAILED. Scoreboard predicted Err mode. Mismatch on %d bit. time = %t", i, $time);
						error_indicator = 1'b1;
					end
				//$display("sout = %b.   predicted_sout.sout_predicted[%d] = %b", sout, i, predicted_sout.sout_predicted[i]);
				end
			end
			else begin : predicted_sout_without_err
				for (shortint i = 53; i>=0; i--) begin      //54 iterations because first start_bit is handled by always (@negedge sout) triggering
					@(posedge bfm.clk);
					if(bfm.sout != predicted_sout.sout_predicted[i]) begin
						//$display("FAILED. Scoreboard predicted NoErr mode. Mismatch on %d bit. time = %t", i, $time);
						error_indicator = 1'b1;
					end
				//$display("sout = %b.   predicted_sout.sout_predicted[%d] = %b", sout, i, predicted_sout.sout_predicted[i]);
				end
			end
	
			if(error_indicator) begin
				$display("FAILED. Time = %t", $time);
			end
			else begin
				$display("PASSED. Time = %t", $time);
			end
		end : self_checker
	endtask : run_phase
////////////////////////////////
endclass : scoreboard