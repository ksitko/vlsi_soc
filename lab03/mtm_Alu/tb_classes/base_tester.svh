virtual class base_tester extends uvm_component;

    `uvm_component_utils(base_tester)

    virtual alu_bfm bfm;

    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        if(!uvm_config_db #(virtual alu_bfm)::get(null, "*","bfm", bfm))
            $fatal(1,"Failed to get BFM");
    endfunction : build_phase
    
    pure virtual function operations_t get_op();
    pure virtual function bit [31:0] get_data();
    
    
	task run_phase(uvm_phase phase);
		bit     [31:0]  A;
		bit     [31:0]  B;
		operations_t    op;
		bit     [3:0]   crc;

		phase.raise_objection(this);
		
		bfm.reset_alu();
		repeat (1000) begin : tester_main
			B   = get_data();
			A   = get_data();
			op  = get_op();
			crc	= bfm.calc_sin_crc(B, A , op);

			bfm.process_two_32bit_numbers(B, A, op, crc);

			#100;
		end : tester_main

		//#1000;
		//$finish;
		phase.drop_objection(this);
	endtask : run_phase

endclass : base_tester