`timescale 1ns / 1ps

module top;

//------------------------------------------------------------------------------
// type and variable definitions
//------------------------------------------------------------------------------

	bit             clk;
	bit             rst_n;
	bit             sin;
	bit             sout;
//------------------------------------------------------------------------------
// DUT instantiation
//------------------------------------------------------------------------------

	mtm_Alu DUT (.*);

//------------------------------------------------------------------------------
// Clock generator
//------------------------------------------------------------------------------

	initial begin : clk_gen
		clk = 0;
		forever begin : clk_frv
			#10;
			clk = ~clk;
		end
	end

//------------------------------------------------------------------------------
// Tester
//------------------------------------------------------------------------------

//----------------------
// Typedefs, parameters and variables
	parameter START_BIT	= 1'b0;
	parameter STOP_BIT  = 1'b1;
	
	parameter T_DATA    = 1'b0;
	parameter T_CMD    	= 1'b1;

	parameter AND		= 3'b000;
	parameter OR		= 3'b001;
	parameter WRONG_1	= 3'b010;
	parameter WRONG_2	= 3'b011;
	parameter ADD		= 3'b100;
	parameter SUB		= 3'b101;
	parameter WRONG_3	= 3'b110;
	parameter WRONG_4	= 3'b111;
	
	bit 		sample_flag;
	
	typedef enum bit {
		data    = T_DATA,
		cmd     = T_CMD
	} t_bit_t;
	
	typedef struct packed {
		bit carry;
		bit overflow;
		bit zero;
		bit negative;
	} flags_t;
	
	typedef struct packed {
		bit err_data;
		bit err_crc;
		bit err_op;
	} err_flags_t;
	
	typedef struct packed {
		bit b7;
		bit b6;
		bit b5;
		bit b4;
		bit b3;
		bit b2;
		bit b1;
		bit b0;
	} byte_t;
	
	typedef struct packed {
		bit zero;
		bit [2:0] op;
		bit [3:0] crc;
	} byte_CMD_t;
	
	typedef struct packed {
		bit zero;
		flags_t flags;
		bit [2:0] crc;
	} byte_C_CMD_t;
	
	typedef struct packed {
		bit one;
		err_flags_t err_flags;
		err_flags_t err_flags_duplicated;
		bit parity;
	} byte_error_C_CMD_t;
	
	typedef union packed {
		byte_t 				bits;
		byte_CMD_t 			CMD_bits;
		bit [7:0] 			whole_byte;
		byte_C_CMD_t		C_CMD_bits;
		byte_error_C_CMD_t	error_C_CMD_bits;
	} payload_u;

	typedef struct packed {
		bit start_bit;      // '0'
		t_bit_t t_bit;      // data='0', cmd='1'
		payload_u payload;  // data
		bit stop_bit;       // '1'
	} packet_t;

	typedef enum bit [2:0] {
		and_op		= AND,
		or_op		= OR,
		wrong_op_1	= WRONG_1,
		wrong_op_2	= WRONG_2,
		add_op		= ADD,
		sub_op		= SUB,
		wrong_op_3	= WRONG_3,
		wrong_op_4	= WRONG_4		
	} operations_t;

	typedef enum bit [2:0] { 
		error_data_lenght	= 3'b000,
		error_cmd_zero_bit	= 3'b001,
		error_sin_crc		= 3'b010,
		error_wrong_op		= 3'b011,
		//error_sout_crc		= 3'b100
		error_no_error		= 3'b111
	} error_t;

	typedef struct packed {
		packet_t	C3;
		packet_t	C2;
		packet_t	C1;
		packet_t	C0;
		packet_t	C_CMD;
	} sout_predicted_t;

	typedef struct {
		bit					err_flag;		//0=no error is predicted, 1=error is predicted
		sout_predicted_t	sout_predicted;	//only CMD if error predicted or C + CMD if no error
	} predicted_queue_element_t;

	bit     [31:0]	A;
	bit		[31:0] 	B;	
	operations_t 	op;
	bit 	[3:0]	crc;
	
	packet_t 		rx_B3, rx_B2, rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD;
	packet_t 		C, C_CMD;
	
	predicted_queue_element_t predicted_values_queue[$];
//----------------------
// Functions

	function bit [3:0] calc_sin_crc(
			input  bit   [31:0]  B,
			input  bit   [31:0]  A,
			input  bit   [2:0]   op
			//output bit   [3:0]   crc
		);
		// polynomial: x^4 + x^1 + 1
		// data width: 68
		// convention: the first serial bit is D[67]

		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;

		begin
			d            = {B, A, 1'b1, op};
			c            = 4'b0000;             //crc init to 0

			newcrc[0]    = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
			newcrc[1]    = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
			newcrc[2]    = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
			newcrc[3]    = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];

			return	newcrc;
		end

	endfunction : calc_sin_crc
////////////////////////////////
	task send_packet(input packet_t packet);
		for (integer i = 0; i<11; i++) begin
			@(posedge clk);
			sin <= packet[10-i];
		end
	endtask
////////////////////////////////
	task DATA(input payload_u payload);
		begin
			@(posedge clk);
			sin <= START_BIT;

			@(posedge clk);
			sin <= T_DATA;

			for (shortint i = 7; i>=0; i--) begin
				@(posedge clk);
				sin <= payload.bits[i];
			end

			@(posedge clk);
			sin <= STOP_BIT;
		end
	endtask
////////////////////////////////
	task CMD(input bit [2:0] op, input bit [3:0] crc);
		begin
			@(posedge clk);
			sin <= START_BIT;

			@(posedge clk);
			sin <= T_CMD;

			@(posedge clk);
			sin <= 1'b0;

			for (shortint i = 2; i>=0; i--) begin
				@(posedge clk);
				sin <= op[i];
			end

			for (shortint i = 3; i>=0; i--) begin
				@(posedge clk);
				sin <= crc[i];
			end

			@(posedge clk);
			sin <= STOP_BIT;
		end
	endtask
////////////////////////////////
	task process_two_32bit_numbers (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [3:0] crc);
		begin			
			DATA(B[31:24]);
			DATA(B[23:16]);
			DATA(B[15:8] );
			DATA(B[7:0]  );
			DATA(A[31:24]);
			DATA(A[23:16]);
			DATA(A[15:8] );
			DATA(A[7:0]  );
			CMD(op, crc);
		end
	endtask
////////////////////////////////
	function operations_t get_op();
		bit [3:0] op_choice;
		op_choice = $random;
		case(op_choice)
			1, 6, 11:	return and_op;
			2, 7, 12:	return or_op;
			3, 8, 13:	return add_op;
			4, 9, 14:	return sub_op;
			0:			return wrong_op_1;
			5:			return wrong_op_2;
			10:			return wrong_op_3;
			15:			return wrong_op_4;
		endcase
	endfunction :get_op
////////////////////////////////
	function bit [31:0] get_data();
		bit [2:0] zero_ones;
		zero_ones = $random;
		if (zero_ones == 3'b000)
			return 32'h0000_0000;
		else if (zero_ones == 3'b111)
			return 32'hFFFF_FFFF;
		else
			return $random;
	endfunction : get_data
//------------------------
// Tester main
	
	initial begin : tester
	
		rst_n   = 1'b0;
		sin 	= 1'b1;
		@(negedge clk);
		@(negedge clk);
		rst_n   = 1'b1;
		@(negedge clk);
		@(negedge clk);

		repeat (1000) begin : tester_main
			B = get_data();
			A = get_data();
			op = get_op();
			crc = calc_sin_crc(B, A , op);
			
			process_two_32bit_numbers(B, A, op, crc);
			
			#100;
		end : tester_main

		#1000;
		$finish;
	end : tester

//------------------------------------------------------------------------------
// Scoreboard
//------------------------------------------------------------------------------
	bit error_indicator;
	
	
	task get_packet(output packet_t out);	//function must be envoked with always @(negedge sin) to properly find start_bit. Reads one packet from sin 
		begin
			//start bit
			if (sin != 1'b0) begin
				@(negedge sin); //always in predict process is already sensitive on negedge sin so that's why this if is here - to not double negedge sin
			end 
			out.start_bit = START_BIT; //no error can be found because sin always will be 0 (=start bit) after @(negedge sin)
			//t bit
			@(posedge clk);	
			@(posedge clk);	//posedge clk x2 because negedge sin was used above
			out.t_bit = t_bit_t'(sin);
			//payload
			for (shortint i=7; i>=0; i--) begin
				@(posedge clk);
				out.payload.bits[i] = sin;
			end
			//stop bit
			@(posedge clk);
			if(sin==STOP_BIT) begin
				out.stop_bit = STOP_BIT;
			end
			else begin
				$error("ERROR: Stop bit was expected while reading packet from sin input. Time = %t", $time);
			end
		end
	endtask
	////////////////////////////////
	task get_packets_for_32bit(output packet_t packet4, output packet_t packet3, output packet_t packet2, output packet_t packet1);
		begin
			get_packet(packet4); //from MSB
			get_packet(packet3);
			get_packet(packet2);
			get_packet(packet1); //to LSB
		end
	endtask
	////////////////////////////////
	task get_CMD(output packet_t out); 
		begin
			get_packet(out);
		end
	endtask
	////////////////////////////////
	task get_B_and_A_and_CMD (	output packet_t rx_B3, output packet_t rx_B2, output packet_t rx_B1, output packet_t rx_B0,
								output packet_t rx_A3, output packet_t rx_A2, output packet_t rx_A1, output packet_t rx_A0,
								output packet_t rx_CMD);
		begin
			get_packets_for_32bit(rx_B3, rx_B2, rx_B1, rx_B0);
			get_packets_for_32bit(rx_A3, rx_A2, rx_A1, rx_A0);
			get_CMD(rx_CMD);
		end
	endtask
	////////////////////////////////
	function flags_t calc_flags (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [31:0] C);
		flags_t flags;
		flags.carry		= 1'b0;
		flags.overflow	= 1'b0;
		flags.zero		= 1'b0;
		flags.negative	= 1'b0;
		begin
			if (op == add_op) begin
				if ((A[31] & B[31]) | (A[31] & ~C[31]) | (B[31] & ~C[31])) 	flags.carry = 1'b1;
				if ((A[31] & B[31] & ~C[31]) | (~A[31] & ~B[31] & C[31])) 	flags.overflow = 1'b1;
			end
			if (op == sub_op) begin
				if ((A[31] & ~B[31]) | (A[31] & C[31]) | (~B[31] & C[31])) 	flags.carry = 1'b1;
				if ((~A[31] & B[31] & ~C[31]) | (A[31] & ~B[31] & C[31])) 	flags.overflow = 1'b1;
			end
			if (C == 32'h0000_0000) 										flags.zero = 1'b1;
			if (C[31] == 1'b1) 												flags.negative = 1'b1;
			
			return flags;
		end
	endfunction 
	////////////////////////////////
	function calc_parity(input packet_t C_CMD);
		return ^C_CMD.payload[7:1];		//reduction XOR
	endfunction 
	////////////////////////////////
	function [2:0] calc_sout_crc(input [31:0] C, input flags_t flags);
		  // polynomial: x^3 + x^1 + 1
		  // data width: 37
		  // convention: the first serial bit is D[36]
		reg [36:0] 	d;
		reg [2:0] 	c;
		reg [2:0] 	newcrc;
	  	
	  	begin
		    d = {C, 1'b0, flags};
		    c = 3'b000;
		
		    newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
		    newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
		    newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
		    return newcrc;
	  	end
	endfunction
	////////////////////////////////
	task enqueue_predicted_result(input bit [31:0] C, input packet_t C_CMD);
		predicted_queue_element_t 	queue_element;
		/* --- Set queue element properly --- */
		if(C_CMD.payload[7] == 1'b1) begin	//by MSB it can be distinguished if error occured or not  
			queue_element.err_flag				= 1'b1;
		end
		else begin
			queue_element.err_flag				= 1'b0;
			queue_element.sout_predicted.C3		= {START_BIT, T_DATA, C[31:24], STOP_BIT};
			queue_element.sout_predicted.C2		= {START_BIT, T_DATA, C[23:16], STOP_BIT};
			queue_element.sout_predicted.C1		= {START_BIT, T_DATA, C[15:8 ], STOP_BIT};
			queue_element.sout_predicted.C0		= {START_BIT, T_DATA, C[7:0  ], STOP_BIT};
		end
		queue_element.sout_predicted.C_CMD	= C_CMD;
		/* --- Enqueue --- */
		predicted_values_queue.push_front(queue_element);
	endtask
	////////////////////////////////
	task predict_op_result (	input packet_t B3, input packet_t B2, input packet_t B1, input packet_t B0,  
								input packet_t A3, input packet_t A2, input packet_t A1, input packet_t A0,
								input packet_t CMD,
								output bit [31:0] C, output packet_t C_CMD);	
		
		flags_t 					flags;
		bit [2:0] 					sout_crc;
		err_flags_t 				err_flags;
		bit	 						parity;
		bit [3:0] 					crc_calculated_from_sin;
		bit [31:0] 					B, A;
		
		begin
			//clear
			flags.carry				= 1'b0;
			flags.overflow			= 1'b0;
			flags.zero				= 1'b0;
			flags.negative			= 1'b0;
			err_flags.err_crc		= 1'b0;
			err_flags.err_op		= 1'b0;
			err_flags.err_data		= 1'b0;
			sout_crc				= 3'b000;
			crc_calculated_from_sin	= 4'b000;
			//parse B and A
			B			= {B3.payload, B2.payload, B1.payload, B0.payload};
			A			= {A3.payload, A2.payload, A1.payload, A0.payload};
			/***************************************/
			/******* Calculating error flags *******/
			/***************************************/
			/* --- ERR_DATA --- */
			if (CMD.t_bit != T_CMD) begin
				err_flags.err_data = 1'b1;		//ERR_DATA. packet number 9 must be CMD, not DATA
			end
			
			if ((B3.t_bit | B2.t_bit | B1.t_bit | B0.t_bit | A3.t_bit | A2.t_bit | A1.t_bit | A0.t_bit) == T_CMD) begin
				err_flags.err_data = 1'b1;		//ERR_DATA. must be exactly 8 DATA packets, not less
			end
			/* --- /ERR_CRC ---*/
			crc_calculated_from_sin = calc_sin_crc(B, A, CMD.payload.CMD_bits.op);
			
			if(crc_calculated_from_sin != CMD.payload.CMD_bits.crc) begin
				err_flags.err_crc = 1'b1;
			end
			/* --- ERR_OP --- */
			if(CMD.payload.CMD_bits.op[1] == 1'b1) begin
				err_flags.err_op = 1'b1;
			end
			/***************************************/
			/********** Predicting result **********/
			/***************************************/
			C_CMD.start_bit = START_BIT;
			C_CMD.t_bit		= t_bit_t'(T_CMD);
			C_CMD.stop_bit	= STOP_BIT;
			if (err_flags != 3'b000) begin
				C_CMD.payload.error_C_CMD_bits.one 					= 1'b1;
				C_CMD.payload.error_C_CMD_bits.err_flags 			= err_flags;
				C_CMD.payload.error_C_CMD_bits.err_flags_duplicated	= err_flags;
				C_CMD.payload.error_C_CMD_bits.parity				= calc_parity(C_CMD);
			end
			else begin
				/* --- Calculate C --- */
				case(CMD.payload.CMD_bits.op)
							AND: 	C = B & A;
							OR:		C = B | A;
							ADD:	C = B + A;
							SUB:	C = B - A;
				endcase
				/* --- Set CMD MSB to zero --- */
				C_CMD.payload.C_CMD_bits.zero	= 1'b0;
				/* --- Calculate flags --- */
				flags							= calc_flags(B, A, op, C);
				C_CMD.payload.C_CMD_bits.flags	= flags;
				/* --- Calculate sout crc --- */
				C_CMD.payload.C_CMD_bits.crc	= calc_sout_crc(C, flags);
			end
			/***************************************/
			/********** Enqueue predicted **********/
			/***************************************/
			enqueue_predicted_result(C, C_CMD);
		end
	endtask	
	

	
	//Predict result and enqueue it
	always @(negedge sin) begin : get_sin_and_enqueue_predicted
		get_B_and_A_and_CMD(rx_B3, rx_B2,rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD);
		sample_flag = ~sample_flag;
		//predict_op_result(rx_B3, rx_B2,rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD, C, C_CMD);
	end : get_sin_and_enqueue_predicted
	
	//------------------------
	// Scoreboard main
		
	always @(posedge sample_flag or negedge sample_flag) begin
		predict_op_result(rx_B3, rx_B2,rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD, C, C_CMD);
	end
	
	always @(negedge sout) begin : scoreboard 		//triggering on negedge sin automatically assumes that first bit is correctly a start_bit == '0' 
		predicted_queue_element_t	predicted_sout; //pop_front from queue to this variable
		
		error_indicator = 1'b0;
		
		predicted_sout = predicted_values_queue.pop_front();	//Dequeue predicted value
		@(posedge clk);		//wait to next (posedge clk) because (negedge sout) was used above and this posedge refers to the same time moment
		
		if (predicted_sout.err_flag == 1'b1) begin : predicted_sout_with_err
			for (shortint i = 9; i>=0; i--) begin 		//10 iterations because first start_bit is handled by always (@negedge sout) triggering and CMD is an 11-bit packet
														//also it's important to iterate on CMD not C3..C0 packets which are all in one structure. CMD is last so it's on LSB bits
				@(posedge clk);
				if(sout != predicted_sout.sout_predicted[i]) begin
					//$display("FAILED. Scoreboard predicted Err mode. Mismatch on %d bit. time = %t", i, $time);
					error_indicator = 1'b1;
				end
				//$display("sout = %b.   predicted_sout.sout_predicted[%d] = %b", sout, i, predicted_sout.sout_predicted[i]);
			end
		end
		else begin : predicted_sout_without_err
			for (shortint i = 53; i>=0; i--) begin		//54 iterations because first start_bit is handled by always (@negedge sout) triggering
				@(posedge clk);
				if(sout != predicted_sout.sout_predicted[i]) begin
					//$display("FAILED. Scoreboard predicted NoErr mode. Mismatch on %d bit. time = %t", i, $time);
					error_indicator = 1'b1;
				end
				//$display("sout = %b.   predicted_sout.sout_predicted[%d] = %b", sout, i, predicted_sout.sout_predicted[i]);
			end
		end		

		if(error_indicator) begin
			$display("FAILED. Time = %t", $time);
		end
		else begin
			$display("PASSED. Time = %t", $time);
		end

	end : scoreboard

//------------------------------------------------------------------------------
// Coverage
//------------------------------------------------------------------------------
	
	bit [2:0] 	cov_op_set;
	packet_t	cov_rx_B3, cov_rx_B2, cov_rx_B1, cov_rx_B0, cov_rx_A3, cov_rx_A2, cov_rx_A1, cov_rx_A0;
	bit [31:0]	cov_A, cov_B;
	
	assign 		cov_B = {rx_B3, rx_B2, rx_B1, rx_B0};
	assign 		cov_A = {rx_A3, rx_A2, rx_A1, rx_A0};
	
	
	always @(posedge sample_flag or negedge sample_flag) begin : get_sin_for_coverage //data is read to rx_* variables in another always and then used in coverage and scoreboard	
		cov_op_set	= rx_CMD.payload.CMD_bits.op;
		cov_rx_B3	= rx_B3;
		cov_rx_B2	= rx_B2;
		cov_rx_B1	= rx_B1;
		cov_rx_B0	= rx_B0;
		cov_rx_A3	= rx_A3;
		cov_rx_A2	= rx_A2;
		cov_rx_A1	= rx_A1;
		cov_rx_A0	= rx_A0;
	end : get_sin_for_coverage
	
	// ---------------------------- //
	//	B - Operations & sequences  //
	// ---------------------------- //
	covergroup op_cov;
		option.name = "cg_op_cov";
		
		//rst: coverpoint rst_n {
		//	bins zero 	= {1'b0};
		//	bins one	= {1'b1};
		//}
		
		coverpoint cov_op_set {
			//A1 Test all operations
			bins A1_all[] = {[and_op : wrong_op_4]};
			
			//A2 Numerical operation then logical 
			bins A2_num_then_logic[] = (add_op, sub_op => and_op, or_op);
			
			//A3 Logical operation then numerical
			bins A3_logic_then_num[] = (and_op, or_op => add_op, sub_op);
			
			//A4 Wrong operation then correct operation
			bins A4_wrong_op_then_corr_op[]	= (wrong_op_1, wrong_op_2, wrong_op_3, wrong_op_4 => and_op, or_op, add_op, sub_op);
			
			//A5 Execute 3 logical operations in row
			bins A5_3_logical_in_row[] 	= ([and_op:or_op]  [* 2]);
			
			//A6 Execute 3 numerical operations in row
			bins A6_3_num_in_row[] 	= ([add_op:sub_op] [* 2]);
			
			//A7 execute all operations after reset
			bins A7_all_after_rst[] = (rst_n => and_op, or_op, add_op, sub_op);
		}
	endgroup 
	// ---------------------------- //
	//	B - Specific data corners   //
	// ---------------------------- //
	covergroup zeros_or_ones_on_ops;
		
		option.name = "cg_zeros_or_ones_on_ops";
 
		all_ops: coverpoint cov_op_set {}
		
		a_leg: coverpoint A {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}
		
		b_leg: coverpoint B {
			bins zeros = {'h0000_0000};
			bins others= {['h0000_0001:'hFFFF_FFFE]};
			bins ones  = {'hFFFF_FFFF};
		}
		
		
		B_op_00_FF: cross a_leg, b_leg, all_ops {
			
			//B1 Simulate all zero input for all the operations
			bins B1_all_ops_00 = binsof (all_ops) && (binsof(a_leg.zeros) || binsof(b_leg.zeros));
			
			//B2 Simulate all one input for all the operations
			bins B1_all_ops_FF = binsof (all_ops) && (binsof(a_leg.ones) || binsof(b_leg.ones));
			
			ignore_bins others_only = binsof(a_leg.others) && binsof(b_leg.others);
		}
		
	endgroup
	
	
	//------------------------
	// Coverage main	
	op_cov oc;
	zeros_or_ones_on_ops c_00_FF;
	
	initial begin : coverage
		oc		= new();
		c_00_FF = new();
		
		forever begin : sample_cov
			@(posedge sample_flag or negedge sample_flag);
			oc.sample();
			c_00_FF.sample();
		end :sample_cov
	end : coverage

endmodule : top
