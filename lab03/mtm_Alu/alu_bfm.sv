interface alu_bfm;
	import alu_pkg::*;

	bit         clk;
	bit         rst_n;
	bit         sin;
	bit			sout;

	bit 		sample_flag;
	predicted_queue_element_t	predicted_values_queue[$];
	
	packet_t 		rx_B3, rx_B2, rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD;
	packet_t 		C, C_CMD;
//------------------------------------------------------------------------------
// Clock generator
//------------------------------------------------------------------------------
	initial begin : clk_gen
		clk = 0;
		forever begin : clk_frv
			#10;
			clk = ~clk;
		end
	end
//------------------------------------------------------------------------------
// Tasks & functions
//------------------------------------------------------------------------------
/////////// From tester: start ///////////
	task reset_alu();
		rst_n   = 1'b0;
		sin     = 1'b1;
		@(negedge clk);
		@(negedge clk);
		rst_n   = 1'b1;
		@(negedge clk);
		@(negedge clk);
	endtask
////////////////////////////////
	function bit [3:0] calc_sin_crc(
			input  bit   [31:0]  B,
			input  bit   [31:0]  A,
			input  bit   [2:0]   op
		);
		// polynomial: x^4 + x^1 + 1
		// data width: 68
		// convention: the first serial bit is D[67]

		reg [67:0] d;
		reg [3:0] c;
		reg [3:0] newcrc;

		begin
			d            = {B, A, 1'b1, op};
			c            = 4'b0000;             //crc init to 0

			newcrc[0]    = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
			newcrc[1]    = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
			newcrc[2]    = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
			newcrc[3]    = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];

			return  newcrc;
		end

	endfunction : calc_sin_crc
////////////////////////////////
	task send_packet(input packet_t packet);
		for (integer i = 0; i<11; i++) begin
			@(posedge clk);
			sin <= packet[10-i];
		end
	endtask
////////////////////////////////
	task DATA(input payload_u payload);
		begin
			@(posedge clk);
			sin <= START_BIT;

			@(posedge clk);
			sin <= T_DATA;

			for (shortint i = 7; i>=0; i--) begin
				@(posedge clk);
				sin <= payload.bits[i];
			end

			@(posedge clk);
			sin <= STOP_BIT;
		end
	endtask
////////////////////////////////
	task CMD(input bit [2:0] op, input bit [3:0] crc);
		begin
			@(posedge clk);
			sin <= START_BIT;

			@(posedge clk);
			sin <= T_CMD;

			@(posedge clk);
			sin <= 1'b0;

			for (shortint i = 2; i>=0; i--) begin
				@(posedge clk);
				sin <= op[i];
			end

			for (shortint i = 3; i>=0; i--) begin
				@(posedge clk);
				sin <= crc[i];
			end

			@(posedge clk);
			sin <= STOP_BIT;
		end
	endtask
////////////////////////////////
	task process_two_32bit_numbers (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [3:0] crc);
		begin
			DATA(B[31:24]);
			DATA(B[23:16]);
			DATA(B[15:8] );
			DATA(B[7:0]  );
			DATA(A[31:24]);
			DATA(A[23:16]);
			DATA(A[15:8] );
			DATA(A[7:0]  );
			CMD(op, crc);
		end
	endtask
/////////// From tester: end ///////////

//////// From scoreboard: start ////////
	task get_packet(output packet_t out);   //function must be envoked with always @(negedge sin) to properly find start_bit. Reads one packet from sin
		begin
			//start bit
			if (sin != 1'b0) begin
				@(negedge sin); //always in predict process is already sensitive on negedge sin so that's why this if is here - to not double negedge sin
			end
			out.start_bit = START_BIT; //no error can be found because sin always will be 0 (=start bit) after @(negedge sin)
			//t bit
			@(posedge clk);
			@(posedge clk); //posedge clk x2 because negedge sin was used above
			out.t_bit     = t_bit_t'(sin);
			//payload
			for (shortint i=7; i>=0; i--) begin
				@(posedge clk);
				out.payload.bits[i] = sin;
			end
			//stop bit
			@(posedge clk);
			if(sin==STOP_BIT) begin
				out.stop_bit = STOP_BIT;
			end
			else begin
				$error("ERROR: Stop bit was expected while reading packet from sin input. Time = %t", $time);
			end
		end
	endtask
////////////////////////////////
	task get_packets_for_32bit(output packet_t packet4, output packet_t packet3, output packet_t packet2, output packet_t packet1);
		begin
			get_packet(packet4); //from MSB
			get_packet(packet3);
			get_packet(packet2);
			get_packet(packet1); //to LSB
		end
	endtask
////////////////////////////////
	task get_CMD(output packet_t out);
		begin
			get_packet(out);
		end
	endtask
////////////////////////////////
	task get_B_and_A_and_CMD (  output packet_t rx_B3, output packet_t rx_B2, output packet_t rx_B1, output packet_t rx_B0,
			output packet_t rx_A3, output packet_t rx_A2, output packet_t rx_A1, output packet_t rx_A0,
			output packet_t rx_CMD);
		begin
			get_packets_for_32bit(rx_B3, rx_B2, rx_B1, rx_B0);
			get_packets_for_32bit(rx_A3, rx_A2, rx_A1, rx_A0);
			get_CMD(rx_CMD);
		end
	endtask
////////////////////////////////
	function flags_t calc_flags (input bit [31:0] B, input bit [31:0] A, input bit [2:0] op, input bit [31:0] C);
		flags_t flags;
		flags.carry     = 1'b0;
		flags.overflow  = 1'b0;
		flags.zero      = 1'b0;
		flags.negative  = 1'b0;
		begin
			if (op == add_op) begin
				if ((A[31] & B[31]) | (A[31] & ~C[31]) | (B[31] & ~C[31]))  flags.carry    = 1'b1;
				if ((A[31] & B[31] & ~C[31]) | (~A[31] & ~B[31] & C[31]))   flags.overflow = 1'b1;
			end
			if (op == sub_op) begin
				if ((A[31] & ~B[31]) | (A[31] & C[31]) | (~B[31] & C[31]))  flags.carry    = 1'b1;
				if ((~A[31] & B[31] & ~C[31]) | (A[31] & ~B[31] & C[31]))   flags.overflow = 1'b1;
			end
			if (C == 32'h0000_0000)                                         flags.zero     = 1'b1;
			if (C[31] == 1'b1)                                              flags.negative = 1'b1;

			return flags;
		end
	endfunction
////////////////////////////////
	function calc_parity(input packet_t C_CMD);
		return ^C_CMD.payload[7:1];     //reduction XOR
	endfunction
////////////////////////////////
	function [2:0] calc_sout_crc(input [31:0] C, input flags_t flags);
		// polynomial: x^3 + x^1 + 1
		// data width: 37
		// convention: the first serial bit is D[36]
		reg [36:0]  d;
		reg [2:0]   c;
		reg [2:0]   newcrc;

		begin
			d         = {C, 1'b0, flags};
			c         = 3'b000;

			newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
			newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
			newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
			return newcrc;
		end
	endfunction
////////////////////////////////
	task enqueue_predicted_result(input bit [31:0] C, input packet_t C_CMD);
		predicted_queue_element_t   queue_element;
		/* --- Set queue element properly --- */
		if(C_CMD.payload[7] == 1'b1) begin  //by MSB it can be distinguished if error occured or not
			queue_element.err_flag              = 1'b1;
		end
		else begin
			queue_element.err_flag              = 1'b0;
			queue_element.sout_predicted.C3     = {START_BIT, T_DATA, C[31:24], STOP_BIT};
			queue_element.sout_predicted.C2     = {START_BIT, T_DATA, C[23:16], STOP_BIT};
			queue_element.sout_predicted.C1     = {START_BIT, T_DATA, C[15:8 ], STOP_BIT};
			queue_element.sout_predicted.C0     = {START_BIT, T_DATA, C[7:0  ], STOP_BIT};
		end
		queue_element.sout_predicted.C_CMD  = C_CMD;
		/* --- Enqueue --- */
		predicted_values_queue.push_front(queue_element);
	endtask
////////////////////////////////
	task predict_op_result (    input packet_t B3, input packet_t B2, input packet_t B1, input packet_t B0,
			input packet_t A3, input packet_t A2, input packet_t A1, input packet_t A0,
			input packet_t CMD,
			output bit [31:0] C, output packet_t C_CMD);

		flags_t                     flags;
		bit [2:0]                   sout_crc;
		err_flags_t                 err_flags;
		bit                         parity;
		bit [3:0]                   crc_calculated_from_sin;
		bit [31:0]                  B, A;

		begin
			//clear
			flags.carry             = 1'b0;
			flags.overflow          = 1'b0;
			flags.zero              = 1'b0;
			flags.negative          = 1'b0;
			err_flags.err_crc       = 1'b0;
			err_flags.err_op        = 1'b0;
			err_flags.err_data      = 1'b0;
			sout_crc                = 3'b000;
			crc_calculated_from_sin = 4'b000;
			//parse B and A
			B                       = {B3.payload, B2.payload, B1.payload, B0.payload};
			A                       = {A3.payload, A2.payload, A1.payload, A0.payload};
			/***************************************/
			/******* Calculating error flags *******/
			/***************************************/
			/* --- ERR_DATA --- */
			if (CMD.t_bit != T_CMD) begin
				err_flags.err_data                                  = 1'b1;      //ERR_DATA. packet number 9 must be CMD, not DATA
			end

			if ((B3.t_bit | B2.t_bit | B1.t_bit | B0.t_bit | A3.t_bit | A2.t_bit | A1.t_bit | A0.t_bit) == T_CMD) begin
				err_flags.err_data                                  = 1'b1;      //ERR_DATA. must be exactly 8 DATA packets, not less
			end
			/* --- /ERR_CRC ---*/
			crc_calculated_from_sin = calc_sin_crc(B, A, CMD.payload.CMD_bits.op);

			if(crc_calculated_from_sin != CMD.payload.CMD_bits.crc) begin
				err_flags.err_crc                                   = 1'b1;
			end
			/* --- ERR_OP --- */
			if(CMD.payload.CMD_bits.op[1] == 1'b1) begin
				err_flags.err_op                                    = 1'b1;
			end
			/***************************************/
			/********** Predicting result **********/
			/***************************************/
			C_CMD.start_bit         = START_BIT;
			C_CMD.t_bit             = t_bit_t'(T_CMD);
			C_CMD.stop_bit          = STOP_BIT;
			if (err_flags != 3'b000) begin
				C_CMD.payload.error_C_CMD_bits.one                  = 1'b1;
				C_CMD.payload.error_C_CMD_bits.err_flags            = err_flags;
				C_CMD.payload.error_C_CMD_bits.err_flags_duplicated = err_flags;
				C_CMD.payload.error_C_CMD_bits.parity               = calc_parity(C_CMD);
			end
			else begin
				/* --- Calculate C --- */
				case(CMD.payload.CMD_bits.op)
					AND:    C = B & A;
					OR:     C = B | A;
					ADD:    C = B + A;
					SUB:    C = B - A;
				endcase
				/* --- Set CMD MSB to zero --- */
				C_CMD.payload.C_CMD_bits.zero                       = 1'b0;
				/* --- Calculate flags --- */
				flags                                               = calc_flags(B, A, CMD.payload.CMD_bits.op, C);
				C_CMD.payload.C_CMD_bits.flags                      = flags;
				/* --- Calculate sout crc --- */
				C_CMD.payload.C_CMD_bits.crc                        = calc_sout_crc(C, flags);
			end
			/***************************************/
			/********** Enqueue predicted **********/
			/***************************************/
			enqueue_predicted_result(C, C_CMD);
		end
	endtask
////////// From scoreboard: end //////////


////////// BFM main: receiving and decoding from sin //////////
	always @(negedge sin) begin : get_sin_and_enqueue_predicted
		get_B_and_A_and_CMD(rx_B3, rx_B2, rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD);
		sample_flag = ~sample_flag;
	end : get_sin_and_enqueue_predicted
/////////////////////////	
	always @(posedge sample_flag or negedge sample_flag) begin
		predict_op_result(rx_B3, rx_B2, rx_B1, rx_B0, rx_A3, rx_A2, rx_A1, rx_A0, rx_CMD, C, C_CMD);
	end
/////////////////////////
endinterface : alu_bfm